# This folder is skeleton to build and publish package

## How to build the package?

Copy your `mathactive_django` folder from inside root `src` directory to root `build-package-here` one.

In terminal being in project root location execute ```cp -r src/mathactive_django build-package-here``` command, after that project structure should look like following (you can utilize `tree` command):
```
.
├── build-package-here
│   ├── MANIFEST.in
│   ├── pyproject.toml
│   ├── README.md
│   ├── setup.cfg
│   └── setup.py
└── src
    ├── core
    │   ├── asgi.py
    │   ├── __init__.py
    │   ├── settings.py
    │   ├── urls.py
    │   └── wsgi.py
    ├── docs
    │   ├── AIMA approaches to creating a chatbot.md
    │   ├── messagebird_api_failed_auth_attempts.hist.ipy
    │   ├── order_control_machine_initial.png
    │   ├── predict_diff.md
    │   └── statemachine_pickle_shelve_failed_attempts.hist.ipy
    ├── manage.py
    ├── mathactive_django
    │   ├── admin.py
    │   ├── apps.py
    │   ├── controllers.py
    │   ├── data
    │   │   ├── difficulty_start_stop_step.csv
    │   │   └── question_templates.yaml
    │   ├── env_loader.py
    │   ├── generators.py
    │   ├── hints.py
    │   ├── __init__.py
    │   ├── logger.py
    │   ├── microlessons
    │   │   └── num_one.py
    │   ├── migrations
    │   │   ├── 0001_initial.py
    │   │   └── __init__.py
    │   ├── models.py
    │   ├── questions.py
    │   ├── urls.py
    │   ├── utils.py
    │   └── views.py
    └── scripts
        ├── bump_version.py
        ├── download_datasets.sh
        ├── download_eedi.sh
        └── requirements.txt
```

Then execute following commands:
```
cd build-package-here
python setup.py sdist
pip install twine
python -m twine upload dist/*
```

After that package should appear/update on pypi

