from django.apps import AppConfig


class MathactiveDjangoConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "mathactive_django"
