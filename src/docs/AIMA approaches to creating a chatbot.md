# AIMA approaches to creating a chatbot

## Student agent approach

This works well with the base Agent class because a tester can pretend to be the student on the command line.

- Teacher(Environment)
- Student(Agent)
    - percept: read TeacherStatement()s

`Teacher(Environment)`s create TeacherStatement objects (questions, congratulations, hints/corrections, instructions).

The squares in the Environment represent locations with the Teacher Statement objects that are positive neutral or negative

- hints/corrections (negative)
- corrections (negative)
- questions(achieve points with congratulations),

### Objects

Like the Scream and Stench objects in the Wumpus world, objects in the conversational world are questions and hings by the Teacher bot (Environment).

- StudentStatement(Object) - possible student utterance
    - Intent(StudentStatement)
    - Answer(StudentStatment)
        - Numbers(Answer)
        - Words(Answer)
- Teacher

## Teacher Agent

- Teacher(Agent)
- Student(Environment)

### Objects

Same as Student Agent approach
